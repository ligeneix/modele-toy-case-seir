#include "./sim_game.h"
#include "./mt19937ar.c"

void print_agents(FILE *stream, const Agent *agents)
{
    int number[4] = {0};
    for(size_t i = 0; i < AGENTS_COUNT; ++i) {
        switch (agents[i].state) {
            case SUSCEPTIBLE:
                number[0]++;
                break;
            case EXPOSED:
                number[1]++;
                break;
            case INFECTED:
                number[2]++;
                break;
            case RECOVERED:
                number[3]++;
                break;
            default:
                break;
        }
    }

        fprintf(stream, "S: %d, E: %d, I: %d, R: %d",
                number[0], number[1], number[2], number[3]);

}


int coord_equals(Coord a, Coord b)
{
    return a.x == b.x && a.y == b.y;
}

double negExp(double inMean)
{
    return -inMean * log(1 - genrand_real2());
}


int random_int_range(int low, int high)
{
    return genrand_int32() % (high - low) + low;
}

void shuffle(Game *game)
{
    for (int i = 0; i < AGENTS_COUNT; ++i) {
        int n = random_int_range(0, AGENTS_COUNT);
        int m = random_int_range(0, AGENTS_COUNT);
        Agent temp = game->agents[n];
        game->agents[n] = game->agents[m];
        game->agents[m] = temp;
    }
}

int is_cell_empty(const Game *game, Coord pos)
{
    for(size_t i = 0; i < AGENTS_COUNT; ++i) {
       if (coord_equals(game->agents[i].pos, pos)) {
           return 0;
       }
    }

    return 1;
}

Coord random_coord_on_grid(void)
{
    Coord result;
    result.x =  random_int_range(0, BOARD_WIDTH);
    result.y =  random_int_range(0, BOARD_WIDTH);
    return result;
}


void place_agent(Game *game, int agent_index)
{
    game->placements[game->agents[agent_index].pos.y][game->agents[agent_index].pos.x]++;
}

void remove_agent(Game *game, int agent_index)
{
    game->placements[game->agents[agent_index].pos.y][game->agents[agent_index].pos.x]--;
}

void calculated(Agent *agent)
{
    agent->d[0] = (int) negExp(3);
    agent->d[1] = (int) negExp(7);
    agent->d[2] = (int) negExp(365);
}

void init_game(Game *game)
{
    for(size_t i = 0; i < BOARD_WIDTH; ++i) {
        for(size_t j = 0; j < BOARD_WIDTH; ++j) {
            game->placements[i][j] = 0;
        }
    }
    game->iterations = 0;
    for(size_t i = 0; i < AGENTS_COUNT - INIT_INFECTIONS; ++i) {
        game->agents[i].pos = random_coord_on_grid();
        game->agents[i].state = SUSCEPTIBLE;
        calculated(&game->agents[i]);
    }

    for(size_t i = 1; i <= INIT_INFECTIONS; ++i) {
        game->agents[AGENTS_COUNT - i].state = INFECTED;
        game->agents[AGENTS_COUNT - i].pos = random_coord_on_grid();
        calculated(&game->agents[AGENTS_COUNT - i]);
        game->agents[AGENTS_COUNT - i].life_span = game->agents[AGENTS_COUNT - i].d[1];
        place_agent(game, AGENTS_COUNT - i);
    }
}

void dump_game(const char *filepath, const Game *game)
{
    FILE *stream = fopen(filepath, "wb");
    if (stream == NULL) {
        fprintf(stderr, "Could not open file '%s'\n", filepath);
        exit(1);
    }
    fwrite(game, sizeof(*game), 1, stream);
    if (ferror(stream)) {
        fprintf(stderr, "Could not dump to file '%s'\n", filepath);
        exit(1);
    }

    fclose(stream);
}

void load_game(const char *filepath, Game *game)
{
    FILE *stream = fopen(filepath, "rb");

    if(stream == NULL) {
        fprintf(stderr, "Could not open file '%s'\n", filepath);
        exit(1);
    }

    size_t n = fread(game, sizeof(*game), 1, stream);
    assert(n == 1);

    if(ferror(stream)) {
        fprintf(stderr, "Could not load from file '%s'\n", filepath);
        exit(1);
    }

    fclose(stream);
}

Env env_of_agent(Game *game, size_t agent_index)
{
    for(size_t i = 0; i < AGENTS_COUNT; ++i) {
        if(coord_equals(game->agents[i].pos, game->agents[agent_index].pos)) {
                return ENV_AGENT;
            }
        }

    return ENV_NOTHING;
}

int count_infected_neighbors(Game *game, size_t agent_index)
{
    int count = 0;

    int offsets[8][2] = {
        {-1, -1}, {0, -1}, {1, -1},
        {-1,  0},          {1,  0},
        {-1,  1}, {0,  1}, {1,  1}
    };

    for (size_t i = 0; i < 8; ++i) {

        int neighborX = (game->agents[agent_index].pos.x + offsets[i][0]
                + BOARD_WIDTH) % BOARD_WIDTH;
        int neighborY = (game->agents[agent_index].pos.y + offsets[i][1]
                + BOARD_WIDTH) % BOARD_WIDTH;

        count += game->placements[neighborY][neighborX];
    }

    return count;

}

void step_agent(Game *game, Agent *agent, int agent_index)
{
    if(agent->state == INFECTED) {
        remove_agent(game, agent_index);
    }
    agent->pos = random_coord_on_grid();
    if(agent->state == INFECTED) {
        place_agent(game, agent_index);
    }
    agent->life_span -= 1;

    switch(agent->state) {
        case EXPOSED:
            if(agent->life_span < 0) {
                agent->state = INFECTED;
                agent->life_span = agent->d[1];
                place_agent(game, agent_index);
            }
            break;
        case INFECTED:
            if(agent->life_span < 0) {
                agent->state = RECOVERED;
                agent->life_span = agent->d[2];
                remove_agent(game, agent_index);
            }
            break;
        case RECOVERED:
            if(agent->life_span < 0) {
                agent->state = SUSCEPTIBLE;
                agent->life_span = 0;
            }
            break;
        default:
            break;
    }
}

void interact_agents(Game *game, size_t agent_index)
{
    Agent *current_agent = &game->agents[agent_index];
    int infected_neighbors = 0;

    infected_neighbors = count_infected_neighbors(game, agent_index);
    double p = 1 - exp(-0.5 * infected_neighbors);

    if(genrand_real2() < p) {
        current_agent->state = EXPOSED;
        current_agent->life_span = current_agent->d[0];
    }
}

void perfom_action(Game *game, size_t agent_index, Action action)
{
    switch (action) {
    case ACTION_STEP:
        step_agent(game, &game->agents[agent_index], agent_index);
        break;
    case ACTION_INTERACTION:
        interact_agents(game, agent_index);
        break;
    }
}

void step_game(Game *game)
{
    for(size_t i = 0; i < AGENTS_COUNT; ++i) {
        if(game->agents[i].state == SUSCEPTIBLE) {
            perfom_action(game, i, ACTION_INTERACTION);
        }
        perfom_action(game, i, ACTION_STEP);
    }
    shuffle(game);
    game->iterations++;
}

int not_max_it(int step_count)
{
    if(step_count == ITERATIONS) {
        return 0;
    }

    return 1;
}
