#ifndef SIM_GAME_H_
#define SIM_GAME_H_

#define BOARD_WIDTH 300
#define BOARD_HEIGHT 300

#define AGENTS_COUNT 20000
#define INIT_INFECTIONS 20
#define ITERATIONS 730

typedef struct {
    int x, y;
} Coord;

int coord_equals(Coord a, Coord b);

typedef enum {
    SUSCEPTIBLE,
    EXPOSED,
    INFECTED,
    RECOVERED
} State;

typedef enum {
    ENV_NOTHING = 0,
    ENV_AGENT,
} Env;

typedef enum
{
    ACTION_STEP = 0,
    ACTION_INTERACTION,
}Action;


typedef struct
{
    Coord pos;
    int d[3];
    int life_span;
    Action action;
    State state;
}Agent;

typedef struct {
    Agent agents[AGENTS_COUNT];
    int placements[BOARD_WIDTH][BOARD_HEIGHT];
    int iterations;
} Game;

int random_int_range(int low, int high);
int is_cell_empty(const Game *game, Coord pos);
Coord random_empty_coord_on_grid(const Game *game);
void init_game(Game *game);
void dump_game(const char *filepath, const Game *game);
void load_game(const char *filepath, Game *game);
Env env_of_agent(Game *game, size_t agent_index);
void step_agent(Agent *agent);
void interact_agents(Game *game, size_t agent_index);
void perfom_action(Game *game, size_t agent_index, Action action);
void step_game(Game *game);
int count_infected_neighbors(Game *game, size_t agent_index);
int not_max_it(int step_count);

#endif
