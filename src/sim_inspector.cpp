#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <SDL.h>

#include "./sim_game.cpp"
#include "./sim_visual.cpp"


Game game = {0};

const char *shift(int *argc, char ***argv)
{
    assert(*argc > 0);
    const char *result = **argv;
    *argc -= 1;
    *argv += 1;
    return result;
}

void usage(FILE *stream)
{
    printf("Usage: ./sim_inspector <input.bin>");
}

int main(int argc, char *argv[])
{
    shift(&argc, &argv);

    if(argc == 0) {
        usage(stderr);
        fprintf(stderr, "ERROR: no input file is provided");
        exit(1);
    }
    const char *input_filepath = shift(&argc, &argv);

    load_game(input_filepath, &game);

    print_agents(stdout, game.agents);

    return 0;
}


