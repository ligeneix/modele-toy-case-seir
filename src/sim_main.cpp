#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <SDL.h>

#include "./sim_game.cpp"
#include "./sim_visual.cpp"
#include "./sim_logging.cpp"

#define null 0

Game game = {0};


#define MAX_GENERATIONS 1000
const char *shift(int *argc, char ***argv)
{
    assert(*argc > 0);
    const char *result = **argv;
    *argc -= 1;
    *argv += 1;
    return result;
}

void usage(FILE *stream)
{
    fprintf(stream, "Usage: ./sim [OPTIONS]\n");
    fprintf(stream, "    -s <SEED>      Seed for random number (default: 420)\n");
    fprintf(stream, "    -g <NUMBER>    Amount of generations (default: 69)\n");
    fprintf(stream, "    -o <FILEPATH>  Output file path (default: output.bin)\n");
    fprintf(stream, "    -gp            Enable epic gnuplot reporting (requires gnuplot, duh)\n");
    fprintf(stream, "    -h             Print this help\n");
}

int main(int argc, char *argv[])
{
    shift(&argc, &argv);


    int generation_count = 69;
    int seed = 420;
    const char *output_filepath = "./data/output";
    int gnuplot = 0;

    while(argc > 0) {
        const char *flag = shift(&argc, &argv);

        if(strcmp(flag, "-s") == 0) {
            if(argc == 0) {
                usage(stderr);
                fprintf(stderr, "ERROR: no value provided fo flag '%s'\n", flag);
                exit(1);
            }
            const char *value = shift(&argc, &argv);
            seed = atoi(value);
        }

        else if(strcmp(flag, "-g") == 0) {
            if(argc == 0) {
                usage(stderr);
                fprintf(stderr, "ERROR: no value provided for flag '%s'\n", flag);
                exit(1);
            }
            const char *value = shift(&argc, &argv);
            generation_count = atoi(value);
        } else if (strcmp(flag, "-o") == 0) {
                if(argc == 0) {
                usage(stderr);
                fprintf(stderr, "ERROR: no value provided for flag '%s'\n", flag);
                exit(1);
            }

            const char *value = shift(&argc, &argv);
            output_filepath = value;
        } else if(strcmp(flag, "-gp") == 0) {
            gnuplot = 1;
        } else if(strcmp(flag, "-h") == 0) {
            usage(stdout);
            exit(0);
        }else {
        usage(stderr);
        fprintf(stderr, "ERROR: unknow flag '%s'\n", flag);
        exit(1);
        }
    }

    init_genrand(seed);

    FILE* log = null;

    if(gnuplot) {
        log = fopen("log.dat", "w+");
        log_header(log);
    }

    char c;

    for(int i = 0; i < generation_count; ++i) {

        char filename[100];
        snprintf(filename, sizeof(filename), "%s_generation_%d_%d.dat", output_filepath, i, seed);

        FILE *output;
        init_game(&game);
        printf("Generation %d...\n", i);
        fflush(stdout);
        if(gnuplot) {
            fclose(log);
            log = fopen("log.dat", "w+");
        }
        else {
            log = fopen(filename, "w");
            log_header(log);
        }
        int step_count = 0;
        while(not_max_it(step_count)) {
            log_generation(log, step_count, &game);
            fflush(log);
            step_game(&game);
            step_count++;
            if(gnuplot) {
                log_live_update();
            }
        }

        if(gnuplot) {
            output = fopen(filename, "w");
            if(output == NULL) {
                perror("Error opening the file");
                exit(1);
            }
            rewind(log);
            while((c = fgetc(log)) != EOF)
            {
                fputc(c, output);
            }
            fclose(output);
        }
        fflush(stdout);


    }


    if(gnuplot) {
        log_close_pipe();
        fclose(log);
    }
    dump_game(output_filepath, &game);

    return 0;
}

