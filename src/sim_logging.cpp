#include "./sim_game.h"

FILE *gnuplot_pipe = 0;

#define nOfGnuplotCmds  9

const char *gnuplotCmds[] = {
    "reset",
    "set size 1,1",
    "set origin 0,0",
    "set xlabel 'Steps'",
    "set ylabel 'Number'",
    "set title 'sim live stats'",
    "set key outside",
    "set grid",
    "plot 'log.dat' using 1:2 with lines lw 2 lt 1 title 'Suspected', 'log.dat' using 1:3 with lines lw 2 lt 2 title 'Exposed', 'log.dat' using 1:4 with lines lw 2 lt 3 title 'Infected', 'log.dat' using 1:5 with lines lw 2 lt 4 title 'Recovered'",
};

void avg_agents(Agent *agents, int result[4])
{

    for (size_t i = 0; i < AGENTS_COUNT; ++i) {
        switch (agents[i].state) {
            case SUSCEPTIBLE:
                result[0]++;
                break;
            case EXPOSED:
                result[1]++;
                break;
            case INFECTED:
                result[2]++;
                break;
            case RECOVERED:
                result[3]++;
                break;
            default:
                break;
        }

    }
}

void log_header(FILE * stream)
{
    fprintf(stream, "#Logging generations\n");
    fprintf(stream,
            "#Generation, number of each state\n");
    gnuplot_pipe = popen("gnuplot -p", "w");
}

void log_generation(FILE *stream, int gen, Game *game)
{
    int number[4] = {0};
    avg_agents(game->agents, number);

    fprintf(stream, "%d, %d, %d, %d, %d\n", gen, number[0],number[1],number[2],number[3]);

}


void log_live_update(void)
{
    for(size_t i = 0; i < nOfGnuplotCmds; ++i) {
        fprintf(gnuplot_pipe, "%s \n", gnuplotCmds[i]);
    }
    fflush(gnuplot_pipe);
}


void log_close_pipe(void)
{

    pclose(gnuplot_pipe);
}

