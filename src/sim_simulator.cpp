#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#include <SDL.h>

#include "./sim_game.cpp"
#include "./sim_visual.cpp"

Game game = {0};

void usage(FILE *stream)
{
    fprintf(stream, "./sim_simulator [input.bin]\n");

}

const char *shift(int *argc, char ***argv)
{
    assert(*argc > 0);
    const char *result = **argv;
    *argc -= 1;
    *argv += 1;
    return result;
}

int main(int argc, char *argv[])
{
    shift(&argc, &argv);

    if(argc == 0) {
        printf("WARNING: No input file is provided. Generating a new random state");
        init_game(&game);
    } else {
        const char *input_filepath = shift(&argc, &argv);
        printf("Loading the state from %s...\n", input_filepath);
        load_game(input_filepath, &game);
    }

    scc(SDL_Init(SDL_INIT_VIDEO));

    SDL_Window *const window = static_cast<SDL_Window*>(scp(SDL_CreateWindow(
            "Test",
            0, 0,
            SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_RESIZABLE)));

    SDL_Renderer *const renderer = static_cast<SDL_Renderer*>(scp(SDL_CreateRenderer(
            window, -1,
            SDL_RENDERER_ACCELERATED)));

    scc(SDL_RenderSetLogicalSize(renderer, SCREEN_WIDTH, SCREEN_HEIGHT));

    int quit = 0;
    while(!quit) {
       SDL_Event event;
       while(SDL_PollEvent(&event)) {
           switch(event.type) {
               case SDL_QUIT: {
                                  quit = 1;
                              }break;
                case SDL_KEYDOWN: {
                        switch (event.key.keysym.sym) {
                            case SDLK_SPACE: {
                                    step_game(&game);
                        }break;
                            case SDLK_r: {
                                    init_game(&game);
                        }break;
                    }
                }break;
           }
       }
       SDL_SetRenderDrawColor(renderer, HEX_COLOR(BACKGROUND_COLOR));
       scc(SDL_SetRenderDrawColor(renderer, 30, 30, 30, 255));
       scc(SDL_RenderClear(renderer));

       render_grid(renderer);
       render_game(renderer, &game);

       SDL_RenderPresent(renderer);
    }

    SDL_Quit();

    return 0;

}
