#ifndef SIM_VISUAL_H_
#define SIM_VISUAL_H_

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 800

#define CELL_WIDTH ((float) SCREEN_WIDTH /(float) BOARD_WIDTH)
#define CELL_HEIGHT ((float) SCREEN_HEIGHT /(float) BOARD_HEIGHT)

#define BACKGROUND_COLOR 0x181818FF
#define GRID_COLOR 0x303030FF
#define AGENT_EXPOSED 0x6B2D5CFF
#define AGENT_INFECTED 0xDA2C38FF
#define AGENT_RECOVER 0x008000FF
#define AGENT_SUSCEPTIBLE 0xD7C0D0FF



int scc(int code);

void *scp(void *ptr);
#define HEX_COLOR(hex)                  \
            ((hex) >> (3 * 8)) & 0xFF,   \
            ((hex) >> (2 * 8)) & 0xFF,   \
            ((hex) >> (1 * 8)) & 0xFF,   \
            ((hex) >> (0 * 8)) & 0xFF



void render_grid(SDL_Renderer *renderer);

void render_agent(SDL_Renderer *renderer, Agent agent);
void render_game(SDL_Renderer *renderer, const Game *game);

#endif // !SIM_VISUAL_H_
