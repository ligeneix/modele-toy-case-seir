#include "./sim_visual.h"

int scc(int code)
{
    if(code < 0) {
        fprintf(stderr, "SDL error: %s\n", SDL_GetError());
        exit(1);
    }

    return code;
}

void *scp(void *ptr)
{
    if(ptr == NULL) {
        fprintf(stderr, "SDL error: %s\n", SDL_GetError());
        exit(1);
    }

    return ptr;
}

void render_grid(SDL_Renderer *renderer)
{
    SDL_SetRenderDrawColor(renderer, HEX_COLOR(GRID_COLOR));
    for(int x = 1; x < BOARD_WIDTH; ++x) {
            scc(SDL_RenderDrawLine(
                        renderer,
                        x * CELL_WIDTH,
                        0,
                        x * CELL_WIDTH,
                        SCREEN_HEIGHT));
    }
    for(int y = 1; y < BOARD_HEIGHT; ++y) {
        scc(SDL_RenderDrawLine(renderer,
                                0,
                                y * CELL_HEIGHT,
                                SCREEN_WIDTH,
                                y * CELL_HEIGHT));
    }
}


void render_agent(SDL_Renderer *renderer, Agent agent)
{

    switch(agent.state) {
        case EXPOSED:
            SDL_SetRenderDrawColor(renderer, HEX_COLOR(AGENT_EXPOSED));
            break;
        case INFECTED:
            SDL_SetRenderDrawColor(renderer, HEX_COLOR(AGENT_INFECTED));
            break;
        case SUSCEPTIBLE:
            SDL_SetRenderDrawColor(renderer, HEX_COLOR(AGENT_SUSCEPTIBLE));
            break;
        case RECOVERED:
            SDL_SetRenderDrawColor(renderer, HEX_COLOR(AGENT_RECOVER));
            break;
        default:
            break;
    }


    SDL_Rect rect = {
        (int) floorf(agent.pos.x * CELL_WIDTH),
        (int) floorf(agent.pos.y * CELL_HEIGHT),
        (int) floorf(CELL_WIDTH),
        (int) floorf(CELL_HEIGHT),
    };

    scc(SDL_RenderFillRect(renderer,
                           &rect));
}

void render_game(SDL_Renderer *renderer, const Game *game)
{

    for(size_t i = 0; i < AGENTS_COUNT; ++i){
        render_agent(renderer, game->agents[i]);
    }
}

