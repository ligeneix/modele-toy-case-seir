# Modèle Toy case SEIR multi-agent

## Dependencies

Devrais être disponible sur Windows, MacOs, et la majorité des distros Linux.

-[SDL2]

## Quick start

```console
$ make
$ ./sim -h
$ ./sim_simulator
```

## sim
Permet de créer des données et de développées des agents sur plusieurs générations.
Le résultat peut être simuler et inspecter par [sim_simulator](#sim_simulator)

## sim_simulator

Permet d'intéragire étape par étape avec la simulation.

#### Controls

| Key              | Action                                                    |
|------------------|-----------------------------------------------------------|
| <kbd>r</kbd>     | Génére une nouvelle génération aléatoire                  |
| <kbd>SPACE</kbd> | Passe a l'étaoe suivante                                  |
