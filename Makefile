PKGS=sdl2
CFLAGS=-Wall -ggdb -std=c++11 -pedantic `pkg-config --cflags sdl2`
LIBS=`pkg-config --libs sdl2` -lm
COMMON_SRC=src/sim_game.h src/sim_game.cpp src/sim_visual.h src/sim_visual.cpp src/mt19937ar.c

all: sim_simulator sim sim_inspector

sim_simulator: src/sim_simulator.cpp $(COMMON_SRC)
	$(CC) $(CFLAGS) -o sim_simulator src/sim_simulator.cpp $(LIBS)

sim: src/sim_main.cpp $(COMMON_SRC)
	$(CC) $(CFLAGS) -o sim src/sim_main.cpp $(LIBS)

sim_inspector: src/sim_inspector.cpp $(COMMON_SRC)
	$(CC) $(CFLAGS) -o sim_inspector src/sim_inspector.cpp $(LIBS)

