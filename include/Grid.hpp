#ifndef __GRID_HPP__
#define __GRID_HPP__

#define GRID_WIDTH 300
#define GRID_HEIGHT 300

#define NB_STATES 4 // S E I R

class Grid
{
    private:
        const int width = GRID_WIDTH;
        const int height = GRID_HEIGHT;

        int states[NB_STATES] = {0};

        std::vector<std::vector<std::list<int>>> zone;

    public:
        Grid (void);
        int getState (int index);
        int getZoneValue(int x, int y);
        void setZoneValue(int x, int y, int value);
        char getStateChar(int state);
        void tick();
};
#endif
